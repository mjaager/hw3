import java.util.LinkedList;

public class DoubleStack {

   public static void main (String[] argum) {
     System.out.println(interpret("2 8 -"));
   }

   private LinkedList<Double> lList;

   DoubleStack() {
      lList = new LinkedList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack temp = new DoubleStack();
      temp.lList = (LinkedList<Double>)lList.clone();
      return temp;
   }

   public boolean stEmpty() {
      return (lList.size() == 0);
   }

   public void push (double a) {
      lList.push(a);
   }

   public double pop() {
      if(!stEmpty()){
         return lList.pop();
      }else {
         throw new RuntimeException("List is empty!");
      }
   } // pop

   public void op (String s) {
      if(lList.size() < 2)
         throw new RuntimeException("Fewer than 2 elements in list!");

      if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")){
         double top2 = lList.pop();
         double top = lList.pop();

         switch(s) {
            case "+":
               push(top + top2);
               break;
            case "-":
               push(top - top2);
               break;
            case "*":
               push(top * top2);
               break;
            case "/":
               push(top / top2);
               break;
         }
      }else{
         throw new RuntimeException("Only operators +,-,*,/ expected, illegal operator: " +s+ " was entered!");
      }
   }
  
   public double tos() {
      if(!stEmpty()){
         return lList.peek();
      }else {
         throw new RuntimeException("List is empty!");
      }
   }

   @Override
   public boolean equals (Object o) {
      if(lList.size() != ((DoubleStack)o).lList.size()){
         return false;
      }else{
         for (int i = 0; i < lList.size()-1; i++) {
            if (lList.get(i) != ((DoubleStack)o).lList.get(i)){
               return false;
            }
         }
         return true;
      }
   }

   @Override
   public String toString() {
      StringBuffer sb = new StringBuffer();
      for (int i = lList.size() - 1; i >=0 ; i--) {
         sb.append(lList.get(i));
      }
      return sb.toString();
   }

   public static double interpret (String pol) {
      if (pol.trim().isEmpty()){
         throw new RuntimeException("Entered string was empty!");
      }

      String[] split = pol.trim().split("\\s+");
      int numbers = 0;
      int operators = 0;
      DoubleStack ds = new DoubleStack();

      for(String s : split) {

         if(doubleCheck(s)) {
            ds.push(Double.parseDouble(s));
            numbers++;
         }else{
            if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
               try {
                  ds.op(s);
               }catch(RuntimeException e){
                  throw new RuntimeException("Fewer than 2 elements in list when trying " +s+ " from expression " +pol);
               }
               operators++;
            }else{
               throw new RuntimeException("Only operators +,-,*,/ expected, operator " +s+ " was used instead in expression " +pol);
            }
         }
      }
      if(!((operators+1) == numbers)) {
         throw new RuntimeException("Numbers count and operators count is not in balance when interpreting expression: " +pol);
      }
      return ds.pop();

   }

   public static boolean doubleCheck(String s) {
      try {
         Double.parseDouble(s);
      } catch(NumberFormatException e) {
         return false;
      }
      return true;
   }

}

